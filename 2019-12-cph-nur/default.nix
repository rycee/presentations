{ pkgs ? import <nixpkgs> {}
, theme ? "white" }:

let

  revealjs = pkgs.fetchFromGitHub {
    owner = "hakimel";
    repo = "reveal.js";
    rev = "3.8.0";
    sha256 = "14cva2hxdv4gxpz2a996qs8xhxffw97a90gkz2mmgdczh1kyn1sc";
  };

in

pkgs.runCommand
  "presentation"
  {
    preferLocalBuild = true;
    allowSubstitutes = false;
    buildInputs = [ pkgs.pandoc ];
  }
  ''
    mkdir -p $out
    cp -r ${revealjs} $out/reveal.js
    pandoc -t revealjs \
           -V theme=${theme} \
           --include-in-header ${./custom-css.html} \
           --slide-level 2 \
           --syntax-definition ${./nix-syntax.xml} \
           --syntax-definition ${./console-syntax.xml} \
           -s ${./presentation.mdwn} \
           -o $out/index.html
  ''

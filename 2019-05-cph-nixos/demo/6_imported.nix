{ config, pkgs, ... }:

{
  programs.git = {
    enable = true;
    userName = "Robert Helgesson";
  };
}

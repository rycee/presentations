{ config, pkgs, ... }:

{
  imports = [ ./4_imported.nix ];

  home.packages = [
    pkgs.bat
    pkgs.cowsay
    pkgs.fd
  ];

  programs.git.userEmail = "robert@chaitsa.com";

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
}

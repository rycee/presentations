{ config, pkgs, ... }:

{
  home.packages = [
    pkgs.bat
    pkgs.cowsay
    pkgs.fd
  ];

  programs.git = {
    enable = true;
    userName = "Robert Helgesson";
    userEmail = "robert@rycee.net";
  };

  programs.msmtp.enable = true;
  programs.mbsync.enable = true;
  accounts.email = {
    accounts = {
      test-account = {
        primary = true;
        address = "home.manager@zoho.eu";
        userName = "home.manager";
        realName = "Home Manager Test Account";
        passwordCommand = "cat ${config.home.homeDirectory}/pass";
        imap.host = "imap.zoho.eu";
        smtp.host = "smtp.zoho.eu";
        msmtp.enable = true;
        mbsync.enable = true;
      };
    };
  };

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;

  services.syncthing.enable = true;
}

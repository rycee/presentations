{ config, pkgs, ... }:

{
  home.packages = [
    pkgs.bat
    pkgs.cowsay
    pkgs.fd
  ];

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
}

{ config, pkgs, ... }:

{
  home.packages = [
    pkgs.bat
    pkgs.cowsay
    pkgs.fd
  ];

  programs.git = {
    enable = true;
    userName = "Robert Helgesson";
    userEmail = "robert@rycee.net";
  };

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
}

{ config, pkgs, ... }:

{
  home.packages = [
    pkgs.bat
    pkgs.cowsay
    pkgs.fd
    pkgs.git
  ];

  home.file.".config/git/config".text = ''
    [user]
    name=Robert Helgesson
    email=robert@rycee.net
  '';

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
}

#! /usr/bin/env nix-shell
#! nix-shell -i bash -p inotify-tools

while
    nix-build --argstr theme black -o result-black
    nix-build --argstr theme white -o result-white
    inotifywait -e close_write \
                custom-css.html default.nix presentation.mdwn
do : ; done

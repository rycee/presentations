let

  # Pinned Nixpkgs to known working commit.
  nixpkgs = builtins.fetchTarball {
   url = "https://github.com/NixOS/nixpkgs/archive/bc8bc2c7cf3066f8a4e0c365651f968195338422.tar.gz";
   sha256 = "130y0yqh1snkadgwdgf5hbdhwh1sz9qw5qi8ji9z3n271f779551";
  };

in

{ pkgs ? import nixpkgs {}
, theme ? "white" }:

let

  revealjs = pkgs.fetchFromGitHub {
    owner = "hakimel";
    repo = "reveal.js";
    rev = "3.8.0";
    sha256 = "14cva2hxdv4gxpz2a996qs8xhxffw97a90gkz2mmgdczh1kyn1sc";
  };

in

pkgs.runCommand
  "presentation"
  {
    preferLocalBuild = true;
    allowSubstitutes = false;
    buildInputs = [
      pkgs.pandoc
      pkgs.haskellPackages.pandoc-emphasize-code
    ];
  }
  ''
    mkdir -p $out
    cp -r ${revealjs} $out/reveal.js
    cp ${./nix-snowflake.svg} $out/nix-snowflake.svg
    pandoc --filter pandoc-emphasize-code \
           -t revealjs \
           -V theme=${theme} \
           --include-in-header ${./custom-css.html} \
           --slide-level 2 \
           --syntax-definition ${./nix-syntax.xml} \
           --syntax-definition ${./console-syntax.xml} \
           -s ${./presentation.mdwn} \
           -o $out/index.html
  ''
